;;; ruby-extra-mode.el --- Provides extra niceties for ruby programming. -*- lexical-binding: t; -*-
;;; Copyright (C) 2019 Samuel First
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Samuel First <samuelfirst@protonmail.com>
;; Version: 0.1.1
;; Package-Requires: ((emacs "24.3"))
;; Keywords: tools, languages, ruby, extra
;; URL: https://gitlab.com/samuelfirst/ruby-extra-mode

;;; Commentary:
;; This package provides the ability to run linters,
;; generate and look up documentation, and load ruby
;; programs into an interactive interpreter.

;;; Code:
;;;###autoload
(defgroup ruby-extra-custom nil
  "Customization group for ruby-extra-mode"
  :group 'programming)

;;;###autoload
(defcustom ruby-extra-linter "ruby-lint.ruby2.6"
  "Linter to use with ruby-extra-lint function."
  :type 'string
  :group 'ruby-extra-custom)

;;;###autoload
(defcustom ruby-extra-doc "ri --no-pager -f 'markdown'"
  "Command to use for opening ruby documentation."
  :type 'string
  :group 'ruby-extra-custom)

;;;###autoload
(defcustom ruby-extra-doc-generator "rdoc"
  "Documentation generator to use."
  :type 'string
  :group 'ruby-extra-custom)

;;;###autoload
(defcustom ruby-extra-ruby-hook nil
  "Non-nil means automatically load ruby-extra-mode whenever `ruby-mode' is loaded."
  :type 'boolean
  :group 'ruby-extra-custom)

;;;###autoload
(defcustom ruby-extra-linter-keywords
  '(("warning" . font-lock-function-name-face)
    ("error" . font-lock-warning-face))
  "Keywords to use when providing syntax highlighting in linter output."
  :type 'cons
  :group 'ruby-extra-custom)

;;;###autoload
(defcustom ruby-extra-doc-gen-keywords
  '((".*%" . font-lock-comment-face)
    ("Classes.*" . font-lock-function-name-face)
    ("Modules.*" . font-lock-variable-name-face)
    ("Constants.*" . font-lock-type-face)
    ("Attributes.*" . font-lock-builtin-face)
    ("Methods.*" . font-lock-doc-face)
    ("Total.*" . font-lock-keyword-face))
  "Keywords to use when providing syntax highlighting in doc output."
  :type 'cons
  :group 'ruby-extra-custom)

(declare-function markdown-mode "ext:markdown-mode.el" nil t)
(declare-function eshell-send-input "ext:eshell.el" nil t)


;;;###autoload
(defun ruby-extra-lint ()
  "Run linter over the program.
This command runs a linter over the file in the current buffer.
It does not pipe the buffer to the linter, so the file must be
saved for correct results.  The default linter is ruby-lint, which
is not included with a default ruby installation, and can be installed
via the gem package manager: `sudo gem install ruby-lint'.  If you
wish to use a different linter, you can define it in the variable
ruby-extra-lint with the command `customize-group ruby-extra-custom'.
The output is shown in `ruby-extra-lint-mode', which colorizes the
output to make it more readable."
  (interactive)
  (shell-command (concat ruby-extra-linter " "
			 (shell-quote-argument
			  (or (buffer-file-name)
			      (error "This buffer has no file"))))
		 "*ruby-extra-lint*"
		 "*ruby-extra-error*")
  (display-buffer "*ruby-extra-lint*")
  (with-current-buffer "*ruby-extra-lint*"
    (ruby-extra-lint-mode)
    (read-only-mode)))


;;;###autoload
(defun ruby-extra-lookup-doc ()
  "Look up documentation for selected text.
If there is no text selected, the word under the cursor will be
used instead.  The documentation will be opened in a split buffer
below the active one.  By default this command uses ri with the
`--no-pager' and `-f 'markdown'' flags.  It will attempt to use
`markdown-mode' for the output if it is installed, however, it is
not required.  The documentation reader to use is stored in the
ruby-extra-doc variable, and can be changed via the command
`customize-group ruby-extra-custom'"
  (interactive)
  (let ((word (if (use-region-p)
		  (buffer-substring-no-properties (mark) (point))
		(thing-at-point 'word 'no-properties))))
    (shell-command (concat ruby-extra-doc " " (shell-quote-argument word))
		   (concat "*ruby-extra-doc-" word "*")
		   "*ruby-extra-error*")
    (display-buffer (concat "*ruby-extra-doc-" word "*"))
    (with-current-buffer (concat "*ruby-extra-doc-" word "*")
	(or (with-demoted-errors "Error: %S"
	      (markdown-mode)
	      (read-only-mode))
	    (special-mode)))))


;;;###autoload
(defun ruby-extra-generate-doc ()
  "Run a documentation generator over a file/directory.
By default this command uses Rdoc; if you wish to use a different
documentation generator, you can change the `ruby-doc-generator'
variable via the command `customize-group ruby-extra-custom'.
Make sure to include any flags you want the command to use.  The
output of the generator is shown in `ruby-extra-doc-gen-mode', which
colorizes the output."
  (interactive)
  (let ((dir (read-file-name "File/Directory: " (file-name-directory
						 (or (buffer-file-name)
						     default-directory)))))
    (shell-command (concat ruby-extra-doc-generator " "
			   (shell-quote-argument dir))
		   "*ruby-extra-doc-generator*"
		   "*ruby-extra-doc-generator-error*")
    (display-buffer "*ruby-extra-doc-generator*")
    (with-current-buffer "*ruby-extra-doc-generator*"
      (ruby-extra-doc-gen-mode))))


;;;###autoload
(define-minor-mode ruby-extra-mode
  "Provide extra niceties for ruby, namely:
* linting
* looking up documentation
* generating documentation

If you want the mode to auto-load when running ruby-mode, you can add
`(add-hook 'ruby-mode-hook 'ruby-extra-mode)' to your .emacs file.

Keymap:
\\{ruby-extra-mode-map}"
  :lighter " ruby-extra"
  :keymap (let ((map (make-sparse-keymap)))
	    (define-key map (kbd "C-d C-v") #'ruby-extra-lint)
	    (define-key map (kbd "C-d C-d") #'ruby-extra-lookup-doc)
	    (define-key map (kbd "C-d C-f") #'ruby-extra-generate-doc)
	    map))


;;;###autoload
(define-derived-mode ruby-extra-lint-mode special-mode
  "ruby-extra-lint-mode"
  "Provide syntax highlighting for linter output.
The keywords used for syntax highlighting are defined in the
`ruby-extra-linter-keywords' variable, and can be modified with
the `customize-group ruby-extra-custom' command."
  (setq font-lock-defaults
	'(ruby-extra-linter-keywords)))


;;;###autoload
(define-derived-mode ruby-extra-doc-gen-mode special-mode
  "ruby-extra-doc-gen-mode"
  "Provide syntax highlighting for documentation generator output.
The keywords used for syntax highlighting are defined in the
`ruby-extra-doc-gen-keywords' variable, and can be modified with the
`customize-group ruby-extra-custom' command."
  (setq font-lock-defaults
	'(ruby-extra-doc-gen-keywords)))

(provide 'ruby-extra-mode)
;;; ruby-extra-mode.el ends here
