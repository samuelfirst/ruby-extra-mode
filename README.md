# Ruby Extra Mode

An Emacs minor mode to provide some niceties that aren't in the default ruby
mode.  This mode is meant to be used along side the major mode, and transform
Emacs into a better Ruby Editor.

## Features

* Linting: The mode allows the user to run a linter of their choice with the
  keyboard shortcut `C-c C-v`.  The linter will run and send output to a
  separate buffer opened in horizontally split view.  Syntax highlighting is
  applied to the buffer to make keywords like 'warning' and 'error' stand out.
* Documentation Reader: Users can open documentation for selected text with
  the keyboard shortcut `C-c C-d`  The documentation is loaded vi ri in the
  markdown format.  If it is available, markdown mode is used to display the
  documentation.
* Documentation Generator: Users can run documentation generators with the
  shortcut `C-c C-g`.  The output of the generator is opened in a horizontally
  split buffer, and syntax-highlighted to make it easier to read.
* Irb Integration: The user can open an interactive ruby shell with their
  program loaded with the shortcut `C-c C-i`.  This allows for quick and easy
  testing.
  
## Installation

Still working on this part, it might be a few weeks before I have time to get
a proper installation method set up.  For now, just use `(load-file)` in your
.emacs file.

## TODO

* Maybe the ability to pipe snippets of code to ruby
* Ruby repl
